package abstractClassExample.cars;

public abstract class Cars {

    private String brand;
    private int topSpeed;
    protected boolean eCar;

    public Cars(String brand, int topSpeed, boolean eCar) {
        this.brand = brand;
        this.topSpeed = topSpeed;
        this.eCar = eCar;
    }


    public String getBrand() {
        return brand;
    }

    public int getTopSpeed() {
        return topSpeed;
    }

    public boolean iseCar() {
        return eCar;
    }

    public abstract String transportationType();
    public abstract String greenChoice();
}
