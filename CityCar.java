package abstractClassExample.cars;

public class CityCar extends Cars {

    public CityCar(String brand, int topSpeed, boolean eCar) {
        super(brand, topSpeed, eCar);
    }

    @Override
    public String transportationType() {
        return "You can transport from 1 up to 4 passengers.";
    }

    @Override
    public String greenChoice() {
        if (eCar == true){ return "You are green enough."; }
        return "Change your car to an eCar by 2030!";
    }
}



