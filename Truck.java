package abstractClassExample.cars;

public class Truck extends Cars {

    private int maxLoad;

    public Truck(String brand, int topSpeed, boolean eCar, int maxLoad) {
        super(brand, topSpeed, eCar);
        this.maxLoad = maxLoad;
    }

    public int getMaxLoad() {
        return maxLoad;
    }

    @Override
    public String toString() {
        return "maxLoad is " + maxLoad;
    }


    @Override
    public String transportationType() {
        return "You can transport freights only!";
    }

    @Override
    public String greenChoice() {
        if (eCar == true){ return "You are green enough."; }
        return "Wait till efficient eTrucks are produced!";
    }
}
