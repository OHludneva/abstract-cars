package abstractClassExample.cars;

import abstractClassExample.cars.CityCar;
import abstractClassExample.cars.Truck;
import abstractClassExample.cars.Minivan;

public class Main {
    public static void main(String[] args) {

        CityCar cityCar = new CityCar("Volkswagen e-up", 130, true);
        System.out.println(cityCar.getBrand());
        System.out.println(cityCar.getTopSpeed());

        System.out.println(cityCar.transportationType());
        System.out.println(cityCar.greenChoice());

        Truck truck = new Truck("Volvo Iron Knight", 276, false, 38);
        System.out.println(truck.getBrand());
        System.out.println(truck.getTopSpeed());
        Cars volvo = new Truck("Volvo Iron Knight", 276, false, 38);
        System.out.println(volvo);

        System.out.println(truck.transportationType());
        System.out.println(truck.greenChoice());

        Minivan minivan = new Minivan("Mercedes Vito", 75, false);
        System.out.println(minivan.getBrand());
        System.out.println(minivan.getTopSpeed());

        System.out.println(minivan.transportationType());
        System.out.println(minivan.greenChoice());
    }
}

