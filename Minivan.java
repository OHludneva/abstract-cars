package abstractClassExample.cars;

public class Minivan extends Cars {

    public Minivan(String brand, int topSpeed, boolean eCar) {
        super(brand, topSpeed, eCar);
    }

    @Override
    public String transportationType() {
        return "You can transport from 1 up to 9 people";
    }

    @Override
    public String greenChoice() {
        if (eCar == true){ return "You are green enough."; }
        return "Change your car to an eCar by 2030!";
    }
}

